FROM registry.dune-project.org/joe/dune-perftool-ci/perftool-base:git
MAINTAINER jorrit@jorrit.de
# install system libraries needed for perftool
USER root

# install prerequisites
RUN set -ex;						\
    export DEBIAN_FRONTEND=noninteractive;		\
    apt-get update;					\
    apt-get install --no-install-recommends --yes	\
      libtbb-dev					\
      ;							\
    apt-get clean;					\
    rm -rf /var/lib/apt/lists/*

# Build clang and the runtime
# Would like to use --shallow-submodules when cloning, but that only works as
# long as the commit the supermodule points to happens to be the tip of a
# branch.  This is (I think) because fetch can only fetch refs, but not
# commits.
RUN set -ex;									\
    cd /tmp;									\
    git clone --depth=1 --recursive						\
	https://zivgitlab.uni-muenster.de/HPC2SE-Project/pacxx-llvm.git;	\
    mkdir build;								\
    cd build;									\
    cmake /tmp/pacxx-llvm							\
	  -DRV_ENABLE_SLEEF=OFF							\
	  -DCMAKE_BUILD_TYPE=Release						\
	  -DBUILD_SHARED_LIBS=ON						\
	  -DLLVM_ENABLE_RTTI=ON							\
	  -DLLVM_ENABLE_CXX1Y=ON						\
	  -DCMAKE_CXX_FLAGS_RELEASE="-O3"					\
	  -DCMAKE_INSTALL_PREFIX=/opt/pacxx;					\
    make -j$(nproc) install;							\
    cd /tmp;									\
    rm -r build pacxx-llvm

# Add links into the default path
RUN set -ex;							\
    ln -s /opt/pacxx/bin/clang /usr/local/bin/pacxx-clang;	\
    ln -s /opt/pacxx/bin/clang++ /usr/local/bin/pacxx-clang++

# Make an opts file
RUN sed 's,/usr/bin/clang,/usr/local/bin/pacxx-clang,g' <opts.clang >opts.pacxx

# Enable Python2 in this image
RUN echo 'CMAKE_FLAGS="$CMAKE_FLAGS -DDUNE_PYTHON_FORCE_PYTHON2=1"' >> opts.pacxx

USER duneci

