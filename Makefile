# The images to build
IMAGES = alugrid:git pdelab:git testtools:git perftool-base:git pacxx-perftool-base:git
# prefix to the plain image names (including registry, if any
PREFIX = registry.dune-project.org/joe/dune-perftool-ci
# external images that need to be pulled
DEPENDENCIES = docker.io/duneci/dune:git

# the default goal
.PHONY: all
all: build

# define dependencies
stamps/build/alugrid+git: stamps/pull/docker.io/duneci/dune+git
stamps/build/pdelab+git: stamps/build/alugrid+git
stamps/build/testtools+git: stamps/build/pdelab+git
stamps/build/perftool-base+git: stamps/build/testtools+git
stamps/build/pacxx-perftool-base+git: stamps/build/perftool-base+git

######################################################################
#
# Plumbing
#

IMAGES_MANGLED = $(subst :,+,$(IMAGES))
DEPENDENCIES_MANGLED = $(subst :,+,$(DEPENDENCIES))

EMPTY =
SPACE = $(EMPTY) $(EMPTY)
TENTATIVE_REGISTRY = $(firstword $(subst /,$(SPACE),$(PREFIX)))
# assume a first component containing '.' is a registry
REGISTRY = $(if $(findstring .,$(TENTATIVE_REGISTRY)),$(TENTATIVE_REGISTRY))

# magic variable, $* must be the mangled image name
DEMANGLED_STEM = $(subst +,:,$*)

.PHONY: login
login:
	docker login $(REGISTRY)

.PHONY: logout
logout:
	docker logout $(REGISTRY)

.PHONY: build
build: $(addprefix stamps/build/,$(IMAGES_MANGLED))

.PHONY: push
push: $(addprefix stamps/push/,$(IMAGES_MANGLED))

.PHONY: pull
pull: $(addprefix stamps/pull/,$(DEPENDENCIES_MANGLED))

.PHONY: clean
clean:
	rm -rf stamps

$(addprefix stamps/build/,$(IMAGES_MANGLED)): stamps/build/%: stamps/last-modified/%
	docker build -t $(PREFIX)/$(DEMANGLED_STEM) $*
	mkdir -p $(dir $@)
	touch $@

$(addprefix stamps/push/,$(IMAGES_MANGLED)): stamps/push/%: stamps/build/%
	docker push $(PREFIX)/$(DEMANGLED_STEM)
	mkdir -p $(dir $@)
	touch $@

$(addprefix stamps/pull/,$(DEPENDENCIES_MANGLED)): stamps/pull/%:
	docker pull $(DEMANGLED_STEM)
	mkdir -p $(dir $@)
	touch $@

# this ensures that the stamps in stamps/last-modified/ are always
# updated to the last date appearing in the corresponding docker build
# dir.  We include stamps/last-modified-dummy as a makefile.  That
# file does not exist, but there is a rule to remake it, so make runs
# that.  In that rule we update the stamp for each individual image.
# But we are careful not to actually use the stamp name as a target
# name, so that we can have the stamp as a prerequisite above without
# actually triggering a rebuild of the stamp.  This ensures that the
# stamps are never remade in the main execution of make, and thus the
# fact that the stamp alwaes need remaking unconditionally does not in
# itself trigger a remake of any built images
-include stamps/last-modified-dummy
.PHONY: stamps/last-modified-dummy $(addprefix stamps/last-modified-dummy/,$(IMAGES_MANGLED))
stamps/last-modified-dummy: $(addprefix stamps/last-modified-dummy/,$(IMAGES_MANGLED))
$(addprefix stamps/last-modified-dummy/,$(IMAGES_MANGLED)): stamps/last-modified-dummy/%:
	mkdir -p $(dir stamps/last-modified/$*)
	touch -d "`find $* -print0 | TZ=UTC+0 xargs -0 stat -c '%y' | sort | tail -n1`" \
	  stamps/last-modified/$*
